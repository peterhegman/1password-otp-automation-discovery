# 1Password OTP automation discovery

1. Set up a Secrets Automation workflow (https://developer.1password.com/docs/connect/get-started)
   - Create a new Vault in 1Password called `Sitespeed`. I used my personal 1Password to test this as I couldn't create a new Vault in GitLab 1Password.
   - Navigate to https://start.1password.com/integrations/connect
   - Create an environnement and choose the `Sitespeed` Vault
   - Create an access token and choose the `Sitespeed` Vault
   - Copy and rename `.env.example` to `.env` and save the access token in the `ACCESS_TOKEN` environnement variable
   - Download `1password-credentials.json`
   - In `.env` set `CREDENTIALS_PATH` to the path of your `1password-credentials.json` file
1. Create a test item in the `Sitespeed` Vault
   - Create a test item in the `Sitespeed` Vault named `Sitespeed setup`
   - Add a new One-time password field and use https://gist.githubusercontent.com/kcramer/c6148fb906e116d84e4bde7b2ab56992/raw/f8e9708ba72625c1fe404f5eb1388d6ed6d443d6/TOTP-AcmeCo-jdoe@example.com.png
1. Run the 1Password Connect Server (https://developer.1password.com/docs/connect/get-started#step-2-deploy-1password-connect-server)
   - Run `docker-compose up` from the working directory of this repository
1. Get information from 1Password (https://developer.1password.com/docs/connect/get-started/#step-3-set-up-applications-and-services-to-get-information-from-1password)
   - Install node modules with `yarn install`
   - Run `node ./index.js`