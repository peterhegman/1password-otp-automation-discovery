require("dotenv").config();
const { OnePasswordConnect } = require("@1password/connect");

const op = OnePasswordConnect({
  serverURL: "http://localhost:8080",
  token: process.env.ACCESS_TOKEN,
  keepAlive: true,
});

const run = async () => {
  const { id: vaultId } = await op.getVault("Sitespeed");
  const item = await op.getItemByTitle(vaultId, "Sitespeed setup");

  const otpField = item.fields.find((field) => field.type === "OTP");

  console.log(otpField.otp);
};

run();
